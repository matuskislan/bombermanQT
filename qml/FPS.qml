import QtQuick 2.0

Item {
    property alias fpsValue: fpsText.text
    id: fps
    width: fpsText.width
    height: fpsText.height
    Text {
        id: fpsText
        anchors.fill: parent
        text: '0'
        font.bold: true
    }
}



