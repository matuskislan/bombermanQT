import QtQuick 2.0

Item {
    id: item1
    focus: true
    Image {
        id: image1
        anchors.fill: parent
        //source: "qrc:/qtquickplugin/images/template_image.png"

    }

    Column {
        id: menu
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20

        spacing: 20

        Rectangle{
            color: "purple"
            height: 40
            width: parent.width
            Text{
                id: createButton
                text: "Create game"
                color: "red"
                anchors.verticalCenter: parent.verticalCenter
            }

            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    console.log('clicked');
                }
                onEntered: {
                    createButton.color = "blue"
                }
                onExited: {
                    createButton.color = "red"
                }
            }
        }

        Text{
            id: createMButton
            text: "Create multiplayer game"
            color: "gray"
            height: 40
        }

        Text{
            id: joinMButton
            text: "Join multiplayer game"
            color: "gray"
            height: 40
        }

        Text{
            id: optionButton
            text: "Options"
            color: "red"
            height: 40
        }

        Text{
            id: creditsButton
            text: "Credits"
            color: "red"
            height: 40
        }

        Text{
            id: exitButton
            text: "Exit"
            color: "red"
            height: 40
        }
    }

    Rectangle {
        id: boxBomb
        color: "blue"
        width: 40
        height: 40
        anchors.right: menu.left
        anchors.rightMargin: 10
        y: 20

    }

    Keys.onPressed: {
        if(event.key === Qt.Key_Up){
            boxBomb.y = boxBomb.y - 60
        }

        if(event.key === Qt.Key_Down){
            boxBomb.y = boxBomb.y + 60
        }

        if(event.key === Qt.Key_Return){
            console.log('return/enter pressed');
        }
    }

}
