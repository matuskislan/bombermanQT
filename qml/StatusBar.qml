import QtQuick 2.0

Item {
    Image{
        id: backgroundImage
        source: "qrc:///resources/images/grassMapStatusBar.png"
        anchors.fill: parent
    }
}
