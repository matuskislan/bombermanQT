import QtQuick 2.0

Item {
    property alias playerImage: playerImage
    id: player
    width: playerImage.width
    height: playerImage.height

    CollisionBox{
        id: bbox
        width: playerImage.width
        height: playerImage.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
    }

    /*
    512 * 640
    8 * 5
    ---------
    64 * 120

    100ms = x
    16ms = 60
      */

    SpriteSequence {
        id: playerImage
        width: 64
        height: 120
        running: true

        Sprite {
            name: "standUp"
            source: "qrc:///resources/spritesheets/player.png"
            frameCount: 1
            frameWidth: 64
            frameHeight: 120
            frameRate: 1
        }
        Sprite {
            name: "standDown"
            source: "qrc:///resources/spritesheets/player.png"
            frameX: 64*2
            frameCount: 1
            frameWidth: 64
            frameHeight: 120
            frameRate: 1
        }
        Sprite {
            name: "standLeft"
            source: "qrc:///resources/spritesheets/player.png"
            frameX: 64*3
            frameCount: 1
            frameWidth: 64
            frameHeight: 120
            frameRate: 1
        }
        Sprite {
            name: "standRight"
            source: "qrc:///resources/spritesheets/player.png"
            frameCount: 1
            frameX: 64
            frameWidth: 64
            frameHeight: 120
            frameRate: 1
        }

        Sprite {
            name: "blue"
            source: "qrc:///resources/spritesheets/player.png"
            frameY: 240
            frameCount: 8
            frameWidth: 64
            frameHeight: 120
            frameRate: 1
        }
    }

    Keys.onPressed: {
        if(event.key === Qt.Key_Up){
            console.log("up");
            playerImage.jumpTo("standUp");
        }
        if(event.key === Qt.Key_Down){
            console.log("down");
            playerImage.jumpTo("standDown");
        }
        if(event.key === Qt.Key_Left){
            console.log("left");
            playerImage.jumpTo("standLeft");
        }
        if(event.key === Qt.Key_Right){
            console.log("right");
            playerImage.jumpTo("standRight");
        }
    }




}
