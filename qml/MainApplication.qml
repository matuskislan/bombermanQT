import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0

ApplicationWindow {
    minimumHeight: 240
    minimumWidth: 320
    width: 1024
    height: 720
    visible: true
    color:'blue'
    //visibility: 'FullScreen'

    Rectangle {
        id: screen
        anchors.fill: parent
        color: 'yellow'

        FPS {
            id: fps
            fpsValue: '60'
            anchors.left: parent.left
            anchors.top: parent.top
            z: 10
        }

        Item {
            id: scene
            width: 640
            height: 480


            property real pw : parent!=null?parent.width:width;
            property real ph : parent!=null?parent.height:height;

            transform: [
                Scale {
                    id: scale; xScale: yScale;
                    yScale: Math.min(
                        scene.pw/scene.width,scene.ph/scene.height);},
                Translate {
                    x: (scene.pw-scene.width*scale.xScale)/2;
                    y: (scene.ph-scene.height*scale.yScale)/2;}
            ]

            Loader {
                id: stateLoader
                anchors.fill: parent

                focus: true
                source: 'MainMenuState.qml'


                Keys.onPressed: {
                    //_mainWindow.buttonClicked(event);
                    event.accepted = true;
                }
            }
        }

    }

}
