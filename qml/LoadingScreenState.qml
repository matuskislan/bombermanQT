import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {

    Image {
        id: background
        source: '../resources/images/loadingScreenBackground.png'
        anchors.fill: parent
    }

    Item {
        anchors.fill: parent

        Text {
            id: textLabel
            text: 'Daj mi resources more'
            anchors.right: progressBar.right
            anchors.bottom: progressBar.top
            anchors.bottomMargin: 10
        }

        ProgressBar {
            id: progressBar
            minimumValue: 9
            maximumValue: 10
            value: 0
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 20

            width: parent.width * 0.8

            style: ProgressBarStyle {
                background: Rectangle {
                    radius: 2
                    color: "orange"
                    border.color: "brown"
                    border.width: 1
                    implicitWidth: 200
                    implicitHeight: 24
                }
                progress: Rectangle {
                    border.color: "red"
                    color: "brown"
                    border.width: 2
                }
            }
        }
    }




}

