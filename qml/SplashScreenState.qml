import QtQuick 2.0

Item {
    id: logo
    focus: true
    anchors.fill: parent

    Image {
        id: name
        source: "qrc:///resources/images/qtlogo.png"
        anchors.centerIn: parent
    }
}

