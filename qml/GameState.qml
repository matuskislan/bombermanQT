import QtQuick 2.0
import QtQuick.Window 2.2

Item {



    width: 640
    height: 480
    //anchors.verticalCenter: parent.verticalCenter
    //transform: Scale { xScale: Screen.width/width; yScale: Screen.height/height}

    StatusBar{
        id: statusBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        //anchors.horizontalCenter: parent.horizontalCenter
        //anchors.centerIn: parent
        height: 50
        //width: 640
    }

    Map {
        id: map1
        objectName: "map1"
        anchors.top: statusBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        //anchors.top: statusBar.bottom
        //anchors.horizontalCenter: parent.horizontalCenter

        //height: 430
        //width: 640


        Wall{
            id:left
            anchors.left:parent.left
            anchors.top: parent.top
            anchors.bottom:parent.bottom
            width:20
        }

        Wall{
            id:right
            anchors.right:parent.right
            anchors.top: parent.top
            anchors.bottom:parent.bottom
            width:20
        }

        Wall{
            id:top
            anchors.top:parent.top
            anchors.left: parent.left
            anchors.right:parent.right
            height:20
        }

        Wall{
            id:bottom
            anchors.bottom:parent.bottom
            anchors.left: parent.left
            anchors.right:parent.right
            height:20
        }


        Scene {
            Player {
                focus:true
                id:player
            }
        }

        MouseArea{
            anchors.fill: parent
            onClicked: console.log('clicked');
        }

    }
}

