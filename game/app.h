#ifndef APP_H
#define APP_H

#include <QObject>
#include <QTimer>
#include <QTime>
#include <QQmlApplicationEngine>

#include "statemanager.h"

class App : public QObject
{
    Q_OBJECT
public:
    explicit App(QQmlApplicationEngine &engine, QObject *parent = 0);
    Q_INVOKABLE void buttonClicked( const QVariant &a);

    void Initialize();

signals:

public slots:
    void Update();


private:
    QTimer timer_;
    QQmlApplicationEngine &engine_;
    StateManager manager_;
    QTime time_;

};

#endif // APP_H
