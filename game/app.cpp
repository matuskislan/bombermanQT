#include "app.h"
#include <QDebug>

App::App(QQmlApplicationEngine &engine, QObject *parent) : QObject(parent),
timer_(this),
engine_(engine),
manager_(engine)
{
    timer_.setTimerType(Qt::PreciseTimer);
    connect(&timer_, SIGNAL(timeout()), this, SLOT(Update()));
}

void App::Initialize(){


    timer_.start(100); //16 => 16ms = 60fps
}

void App::Update() {
    int msElapsed = time_.restart();
    manager_.Update(msElapsed);
}

void App::buttonClicked(const QVariant &a){

    qDebug() << a;
}


