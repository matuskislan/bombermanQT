#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <QQmlApplicationEngine>

#include <memory>

#include "enums/enumstates.h"
#include "states/state.h"



class StateManager
{
public:
    StateManager(QQmlApplicationEngine &engine);
    void Update(int);


private:
    State *current_state_;
    std::map<EnumStates, std::shared_ptr<State>> states_;
    bool running_;
    bool paused_;

    QQmlApplicationEngine &engine_;
};

#endif // STATEMANAGER_H
