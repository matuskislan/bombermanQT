#ifndef ENUMSTATES
#define ENUMSTATES

enum class EnumStates {
    SPLASH,
    MAINMENU,
    NEWGAME,
    GAMELOADING,
    GAME,
    NEWMULTI,
    JOINMULTI,
    OPTIONS,
    ABOUT,
    EXIT
};

#endif // ENUMSTATES

