#ifndef STATE_H
#define STATE_H

#include <QQmlApplicationEngine>

#include "game/enums/enumstates.h"

class StateManager;

class State : public QObject
{
public:
    State(QQmlApplicationEngine &engine, StateManager &manager);
    virtual void Update(int) = 0;

    virtual void Pause() = 0;
    virtual void Resume() = 0;

    virtual void Initialize() = 0;

    virtual EnumStates getStateType() { return stateType_; }

protected:
    QQmlApplicationEngine &engine_;
    EnumStates stateType_;
    StateManager &manager_;
};

#endif // STATE_H
