#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QColor>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTime>
#include <QQmlContext>

#include "game/app.h"

int main(int argc, char *argv[]) {

    int ret = 0;
    QApplication app(argc, argv);
    app.setApplicationName("BomberMan $4000$");
    app.setApplicationVersion("0.1.0");
    app.setOrganizationDomain("bombermanis.com");
    app.setOrganizationName("SmakerLand");

    QQmlApplicationEngine engine;

    App game(engine);

    game.Initialize();



//    qDebug() << "rootobjects length: " << engine.rootObjects().length();

//    QQuickWindow *window = qobject_cast<QQuickWindow*>(engine.rootObjects().at(0));
//    if (!window) {
//        qFatal("Error: Your root item has to be a window.");
//        return -1;
//    }
//    //window->show();
//    QQuickItem *root = window->contentItem();

//    qDebug() << engine.rootObjects().at(0)->findChild<QQuickItem *>("map1");

//    QQmlComponent component(&engine, QUrl(QStringLiteral("qrc:/qml/Bomb.qml")));
//    qDebug() << "component create status:" << component.status() << component.errors();
//    QQuickItem *object = qobject_cast<QQuickItem*>(component.create());

//    QQmlEngine::setObjectOwnership(object, QQmlEngine::CppOwnership);

//    qDebug() << engine.children();
//    QQuickItem *map = engine.rootObjects().at(0)->findChild<QQuickItem *>("map1");

//    qDebug() << map;
//    object->setParentItem(root);
//    object->setProperty("color","yellow");
//    object->setParentItem(map);
//    object->setParent(&engine);


    ret = app.exec();

    qDebug() << "uz po execu";

    return ret;
}
